import React from 'react';
import { App, MainScreen } from './App';
import PostModel from './lib/models';

import renderer from 'react-test-renderer';

describe("MainScreen", () => {
  let props;
  let renderedJSON;
  const rendered = () => {
    if (!renderedJSON)
      renderedJSON = renderer.create(<MainScreen {...props} />).toJSON();
    return renderedJSON;
  };

  beforeEach(() => {
    props = {
      posts: [
        new PostModel("post 1"),
        new PostModel("post 2"),
        new PostModel("post 3"),
        new PostModel("post 4"),
        new PostModel("post 5"),
        new PostModel("post 6"),
        new PostModel("post 7"),
      ]
    }
    renderedJSON = undefined;
  });

  it('renders without crashing', () => {
    expect(rendered()).toBeTruthy();
  });

  it('renders list', () => {
    expect(rendered().children[1].type).toEqual("RCTScrollView");
    expect(rendered().children[1].children[0].children.length).toEqual(7);
  })

});
