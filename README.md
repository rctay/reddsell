A mobile app written in React Native for submitting posts and voting up and down.

To run, `npm start`.

To run tests, `npm test`.

This project was bootstrapped with [Create React Native App](https://github.com/react-community/create-react-native-app).

