export default class PostModel {
	constructor(title, up_votes=0, down_votes=0) {
		this.title = title;
		this.up_votes = up_votes;
		this.down_votes = down_votes;
	}
}

