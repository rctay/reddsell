import React from 'react';
import { StyleSheet, Text, TextInput, Button, View, FlatList } from 'react-native';
import PostModel from './lib/models';


export default class App extends React.Component {
  render() {
    return (<MainScreen posts={
      [
        new PostModel('Prince Charles is nearly 5 years older than the UK men’s retirement age, and he’s not even started the job he was born to do. (self.Showerthoughts)', /*up_votes*/ 36, /* down_votes */ 0),
        new PostModel('TIL that you are guaranteed a lawyer if you cannot afford one (in the U.S.) because a poor man was convicted unjustly, began reading law in prison, and wrote the Supreme Court, saying that he should have been given a lawyer despite his lack of money. He was retried and acquitted. (en.wikipedia.org)', /*up_votes*/ 33, /* down_votes */ 0),
      ]
    }/>);
  }
}

export class MainScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      post_input: '',
      posts: props.posts,
      submitted_empty: false,
    };
  }

  _onPressPostButton() {
    if (this.state.post_input.trim().length <= 0) {
      this.setState({submitted_empty: true});
      return;
    }
    // React does not allow state to be mutated (ie. this.state.posts), so we
    // exploit Array.concat() creates a copy
    var new_posts = [ new PostModel(this.state.post_input) ].concat(this.state.posts);
    this.setState({posts: new_posts, post_input: ""});
  }

  _onPressUpvoteButton(key) {
    var new_posts = this.state.posts
      .map(item => {
        if (item.key === key)
          item.up_votes += 1;
        return item;})
    this.setState({posts: new_posts});
  }

  _onPressDownvoteButton(key) {
    var new_posts = this.state.posts
      .map(item => {
        if (item.key === key)
          item.down_votes += 1;
        return item;})
    this.setState({posts: new_posts});
  }

  getListData() {
    var nett_votes = x => x.up_votes - x.down_votes;
    return this.state.posts
      .map(x => { x.key = x.title; return x})
      .sort((x,y) => nett_votes(y) - nett_votes(x));
  }

  getTextInputPlaceholderStyle(styles) {
    if (this.state.submitted_empty)
      return input_error_placeholderTextColor;
    else
      return input_normal_placeholderTextColor;
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.input_label}>
            Enter your story submission below:
          </Text>
          <TextInput
            placeholder="Enter story here"
            placeholderTextColor={ this.getTextInputPlaceholderStyle(styles) }
            maxLength={ 255 }
            onChangeText={ text => this.setState({post_input: text, submitted_empty: false}) }
            value={ this.state.post_input }
          />
          <Button
            onPress={ this._onPressPostButton.bind(this) }
            title="Post"
          />
        </View>
        <FlatList
          style={styles.list}
          data={ this.getListData() }
          renderItem={({item}) =>
            <View style={styles.item_view}>
              <Text style={styles.item_votes}>Upvotes: {item.up_votes} Downvotes: {item.down_votes}</Text><Text style={styles.item_text}>{item.title}</Text>
              <View style={styles.voting_container}>
                <Button style={styles.voting_button} onPress={ this._onPressUpvoteButton.bind(this, item.key) } title="Upvote" />
                <Button style={styles.voting_button} onPress={ this._onPressDownvoteButton.bind(this, item.key) } title="Downvote" />
              </View>
            </View>}
        />
      </View>
    );
  }
}

const input_normal_placeholderTextColor = '#ccc';
const input_error_placeholderTextColor = '#f00';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {

  },
  input_label: {

  },
  list: {

  },
  item_view: {

  },
  item_votes: {

  },
  voting_container: {
    margin: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  voting_button: {

  },
  item_text: {
    marginLeft: 20,
  },
});
